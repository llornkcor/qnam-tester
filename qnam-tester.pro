#-------------------------------------------------
#
# Project created by QtCreator 2015-05-20T15:32:21
#
#-------------------------------------------------

QT       += core gui network
CONFIG += debug
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qnam-tester
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    qname-tester.qrc
