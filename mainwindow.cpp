#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkConfiguration>
#include <QNetworkConfigurationManager>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    downloadTImer(0)
{
    ui->setupUi(this);
    manager = new QNetworkAccessManager(this);
   // qDebug() << manager->networkAccessible();

    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));

    doDownload();
 //   QTimer::singleShot(downloadTImer, this, SLOT(doDownload()));
//    QNetworkRequest request0a;
//    request0a.setUrl(QUrl("http://10.0.0.11:8000/blah"));
//    QNetworkReply *reply0a = manager->get(request0a);
    //    connectReply(reply0a);
//    if (reply0a->error())
//        qDebug() << Q_FUNC_INFO << reply0a->errorString();


    //// https://www.html5rocks.com/static/images/cors_flow.png

//    QNetworkRequest request0;
//    request0.setUrl(QUrl("http://html5rocks-cors.s3-website-us-east-1.amazonaws.com/index.html"));
//    QNetworkReply *reply0 = manager->get(request0);
//        connectReply(reply0);
//    if (reply0->error())
//        qDebug() << Q_FUNC_INFO << reply0->errorString();

//    QTimer::singleShot(100, reply0, SLOT(abort()));

//    QNetworkRequest request;
//    request.setUrl(QUrl("http://10.0.0.12/openlogo-75.png"));
//    QNetworkReply *reply = manager->get(request);
//    connectReply(reply);
//    if (reply->error())
//        qDebug() << Q_FUNC_INFO << reply->errorString();


//   qDebug() << manager->activeConfiguration().identifier()
//            << manager->activeConfiguration().bearerTypeName()
//            << manager->activeConfiguration().bearerType()
//            << manager->activeConfiguration().name()
//            << manager->activeConfiguration().state();


//    QNetworkRequest request3;
//    request3.setUrl(QUrl("http://ip.jsontest.com/"));
//    request3.setAttribute(QNetworkRequest::CookieLoadControlAttribute, QNetworkRequest::Manual);
//    qDebug() << Q_FUNC_INFO << "CookieLoadControlAttribute" << request3.attribute(QNetworkRequest::CookieLoadControlAttribute);

//    request3.setRawHeader("Content-Type", "application/json");
//    QNetworkReply *reply3 = manager->get(request3);
    //    connectReply(reply3);
//    if (reply3->error())
//        qDebug() << Q_FUNC_INFO << reply3->errorString();


//    QNetworkRequest requestP;
//    requestP.setUrl(QUrl("http://10.0.0.12/echojson.php"));
//    requestP.setRawHeader("Content-Type", "application/json");
//    QJsonObject json;
//    json.insert("item1", "value1");
//    json.insert("item2", "value2");
//    QByteArray jsonDoc = QJsonDocument(json).toJson();
//    qDebug() << Q_FUNC_INFO << jsonDoc.size() << jsonDoc;
//    QNetworkReply *replyP = manager->post(requestP, jsonDoc);
    //    connectReply(replyP);
//    if (replyP->error())
//        qDebug() << Q_FUNC_INFO << replyP->errorString();

    //    QByteArray data = QtJson::Json::serialize(collectSyncData());

// xmlhttp.send(JSON.stringify({name:"John Rambo", time:"2pm"}));}}}}

//    QNetworkRequest requestR;
//    requestR.setUrl(QUrl("http://10.0.0.12/redirect1.html"));
//    requestR.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
//    QNetworkReply *replyR = manager->get(requestR);
//    connectReply(replyR);
//    if (replyR->error())
//        qDebug() << Q_FUNC_INFO << replyR->errorString();

//    QNetworkRequest requestF;
//    requestF.setUrl(QUrl("http://192.168.0.7/smartgit-18_1_4.deb"));
//    QNetworkReply *replyF = manager->get(requestF);
//    connectReply(replyF);
//    if (replyF->error())
//        qDebug() << Q_FUNC_INFO << replyF->errorString();
//    QTimer::singleShot(250, replyF, SLOT(abort()));


//    QFile myFile(":/cheese.jpg");
//    QByteArray putData;

//   if (myFile.open(QIODevice::ReadOnly)) {

//    putData.append(myFile.readAll());

//    qDebug() << Q_FUNC_INFO << putData.size();
//        QNetworkRequest requestPut;
//        requestPut.setUrl(QUrl("http://10.0.0.12/filehandler.php"));
//        QNetworkReply *replyPut = manager->put(requestPut,putData);
//        connectReply(replyPut);
//        connect(replyPut, SIGNAL(uploadProgress(qint64,qint64)), this,SLOT(uploadProgress(qint64,qint64)));
//        if (replyPut->error())
//            qDebug() << Q_FUNC_INFO << replyPut->errorString();
//   } else {
//       qDebug() << Q_FUNC_INFO << "<<<< FAILED >>>>>>>>>>>>>>>>";
//   }

}

void MainWindow::doDownload()
{
    QNetworkRequest requestF;
    requestF.setUrl(QUrl("http://192.168.1.124/index.html"));
    replyF = manager->get(requestF);
    connectReply(replyF);
    if (replyF->error())
        qDebug() << Q_FUNC_INFO << replyF->errorString();

//    replyF->abort();
//    QTimer::singleShot(downloadTImer, replyF, SLOT(abort()));
//    downloadTImer += 75;
//    QTimer::singleShot(downloadTImer, this, SLOT(doDownload()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectReply(QNetworkReply *reply)
{
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this,SLOT(downloadProgress(qint64,qint64)));
    connect(reply, SIGNAL(uploadProgress(qint64,qint64)),this,SLOT(uploadProgress(qint64,qint64)));
    connect(reply, SIGNAL(finished()),this,SLOT(finished()));
    connect(reply, SIGNAL(redirected(QUrl)),this,SLOT(redirected(QUrl)));
    connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error),
        [=](QNetworkReply::NetworkError code){
    qDebug() << Q_FUNC_INFO << code << reply->errorString();
    });


}

void MainWindow::replyFinished(QNetworkReply *reply)
{
    QVariant statusCodeV =
           reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);

    qDebug() << Q_FUNC_INFO << "status code" << statusCodeV;

    if (reply->error())
        qDebug() << Q_FUNC_INFO  << reply->errorString()
                 << reply->rawHeaderList();
    else {

        QByteArray response_data = reply->readAll();
        qDebug() << Q_FUNC_INFO << response_data;
//        QFile *file = new QFile("smartgit-18_1_4.deb");
//        file.open(QIODevice::WriteOnly);

//        file.write(response_data);
//        file.close();

        qDebug() << qChecksum(response_data, 1024);

        qDebug() << Q_FUNC_INFO  << "all ok";
        qDebug() << Q_FUNC_INFO  << response_data.size();

        qDebug() <<"";
        //    qDebug() << reply->rawHeaderList();
        QList<QByteArray> headerList = reply->rawHeaderList();
        foreach(QByteArray head, headerList) {
            qDebug() << head << ":" << reply->rawHeader(head);
            if (reply->rawHeader(head).contains("json")) {
                QJsonDocument json = QJsonDocument::fromJson(response_data);
                qDebug() << json;
            }
        }
    }
    reply->deleteLater();
}

void MainWindow::redirected(const QUrl &url)
{
qDebug() << Q_FUNC_INFO << url.url();
}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesReceived);
    ui->progressBar->setFormat("%v of %m");
//qDebug() << Q_FUNC_INFO << "bytesReceived" << bytesReceived << "bytesTotal" << bytesTotal;
}

void MainWindow::uploadProgress(qint64 bytesSent, qint64 bytesTotal)
{
    qDebug() << Q_FUNC_INFO << "bytesSent" << bytesSent << "bytesTotal" << bytesTotal;
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesSent);
    ui->progressBar->setFormat("%v of %m");
}

void MainWindow::finished()
{
    qWarning() << "Error state:" << replyF->error() << replyF->errorString();
    printf("%s\n", replyF->readAll().constData());
    replyF->deleteLater();
}


void MainWindow::networkReplyError(QNetworkReply::NetworkError code)
{
    qDebug() << Q_FUNC_INFO << code;
}
