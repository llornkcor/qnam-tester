#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;

    void connectReply(QNetworkReply *reply);
    int downloadTImer;
    QNetworkReply *replyF;

private slots:
    void replyFinished(QNetworkReply*);

    void redirectAllowed()  { qDebug() << Q_FUNC_INFO; }
    void redirected(const QUrl &url);
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void uploadProgress(qint64 bytesSent, qint64 bytesTotal);
    void metaDataChanged() { qDebug() << Q_FUNC_INFO; }
    void finished();
    void networkReplyError(QNetworkReply::NetworkError code);

    void doDownload();

};

#endif // MAINWINDOW_H
